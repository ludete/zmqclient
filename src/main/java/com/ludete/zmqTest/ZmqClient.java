package com.ludete.zmqTest;

/**
 * Hello world!
 *
 */

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZMQ.Poller;

import com.google.common.primitives.Ints;

public class ZmqClient{

    public static void main(String[] args) {
        Context context = ZMQ.context(1);

        //  Socket to talk to server
        Socket requester = context.socket(ZMQ.SUB);
        requester.connect("tcp://127.0.0.1:5567"); // REF ABOVE AND LET START THIS AFTER "server"

//        requester.setReceiveTimeOut(6000);
        System.out.println("launch and connect client.");


//        requester.subscribe("*".getBytes(ZMQ.CHARSET));
        requester.subscribe("-1".getBytes());
        while(true){

            byte[] reply = requester.recv();

//            String reply  = requester.recv();
            if(reply != null){
                System.out.println("Received reply " + " [" + new String(reply) + "]");
            }else {
                System.out.println("Not Received msg from ZMQ !");
            }

        }
    }
}

